from django.urls import path
from todos.views import (
    todolist_list,
    show_todolist,
    todo_list_create,
    update_todolist,
    delete_list,
    create_item,
    update_item,
)

urlpatterns = [
    path("", todolist_list, name="todo_list_list"),
    path("<int:id>", show_todolist, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
]
