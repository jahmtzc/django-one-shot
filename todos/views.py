from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todolist_list(request):
    todolists = TodoList.objects.all()

    context = {"todolist_list": todolists}
    return render(request, "todos/list.html", context)


def show_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todolist,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def update_todolist(request, id):
    list_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance=list_instance)

    context = {
        "update_form": form,
        "list_object": list_instance,
    }

    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()

    context = {"item_form": form}

    return render(request, "todos/itemcreate.html", context)


def update_item(request, id):
    list_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list_instance)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=list_instance)

    context = {
        "item_update_form": form,
        "itemupdate_object": list_instance,
    }

    return render(request, "todos/itemedit.html", context)
